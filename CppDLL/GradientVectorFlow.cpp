#include "mist/mist.h"
#include "mist/vector.h"
#include <algorithm>
#include <vector>
#include <fstream>

extern "C"
{
	typedef void(__stdcall * ProgressCallback)(const char*);

	__declspec(dllexport) bool Generate(ProgressCallback callbackFunction, const mist::array3<short>* inputImage, mist::array3<float>* vectorX, mist::array3<float>* vectorY, mist::array3<float>* vectorZ, int iterations, float mu /*mist::array3<short>* tempOutput*/);
}

typedef mist::array3 < mist::vector3<float> >  VectorFieldType;


void NormalizeIntensity(mist::array3<float>& image)
{
	auto minmaxElem = minmax_element(image.begin(), image.end());
	auto minElem = *(minmaxElem.first);
	auto maxElem = *(minmaxElem.second);

	for_each(image.begin(), image.end(), [minElem, maxElem](float& value){
		value = static_cast<float>(value - minElem) / (minElem + maxElem);
	});
}

//Generate gradient of image.
bool createVectorField(const mist::array3<short>* inputImage, VectorFieldType& initialVectorField)
{
	auto normalizedImage = (mist::array3<short>::rebind<float>::other)(*inputImage);
	NormalizeIntensity(normalizedImage);

	initialVectorField.resize(normalizedImage.size1(), normalizedImage.size2(), normalizedImage.size3());
	initialVectorField.fill(mist::vector3<float>(0, 0, 0));
	for (size_t x = 1; x < normalizedImage.size1() - 1; ++x)
	{
		for (size_t y = 1; y < normalizedImage.size2() - 1; ++y) 
		{
			for (size_t z = 1; z < normalizedImage.size3() - 1; ++z) 
			{
				mist::vector3<float> vector;
				vector.x = 0.5*((normalizedImage)(x + 1, y, z) - (normalizedImage)(x - 1, y, z));
				vector.y = 0.5*((normalizedImage)(x, y + 1, z) - (normalizedImage)(x, y - 1, z));
				vector.z = 0.5*((normalizedImage)(x, y, z + 1) - (normalizedImage)(x, y, z - 1));

				initialVectorField(x, y, z) = vector;
			}
		}
	}
	return true;
}

bool Generate(ProgressCallback callbackFunction, const mist::array3<short>* inputImage, mist::array3<float>* vectorX, mist::array3<float>* vectorY, mist::array3<float>* vectorZ, int iterations, float mu /*, mist::array3<short>* tempOutput*/)
{
	VectorFieldType initialVectorField;
	createVectorField(inputImage, initialVectorField);

	VectorFieldType gvField(initialVectorField);

	for (int i = 0; i < iterations; ++i)
	{
		mist::array3 < mist::vector3<float> > newGvField(inputImage->size1(), inputImage->size2(), inputImage->size3());
		for (size_t z = 0; z < gvField.size3(); ++z)
		{
			for (size_t y = 0; y < gvField.size2(); ++y)
			{
				for (size_t x = 0; x < gvField.size1(); ++x)
				{
					size_t inner_x = x, inner_y = y, inner_z = z;
					// Neumann boundary conditions (if a coordinate is 0, change it to 2. If coordinate is equal to the size-1 move it to size-3)
					inner_x = (x == 0 ? 2 : inner_x);
					inner_y = (y == 0 ? 2 : inner_y);
					inner_z = (z == 0 ? 2 : inner_z);
					inner_x = (x == gvField.size1() - 1 ? gvField.size1() - 3 : inner_x);
					inner_y = (y == gvField.size2() - 1 ? gvField.size2() - 3 : inner_y);
					inner_z = (z == gvField.size3() - 1 ? gvField.size3() - 3 : inner_z);

					mist::vector3<float> v = gvField(inner_x, inner_y, inner_z);
					mist::vector3<float> f = initialVectorField(inner_x, inner_y, inner_z);
					mist::vector3<float> laplacian = -6*v + 
						gvField(inner_x + 1, inner_y + 0, inner_z + 0) +
						gvField(inner_x + 0, inner_y + 1, inner_z + 0) +
						gvField(inner_x + 0, inner_y + 0, inner_z + 1) +
						gvField(inner_x - 1, inner_y - 0, inner_z - 0) +
						gvField(inner_x - 0, inner_y - 1, inner_z - 0) +
						gvField(inner_x - 0, inner_y - 0, inner_z - 1);
					
					newGvField(inner_x, inner_y, inner_z) = v + mu*laplacian - (f.x*f.x + f.y*f.y + f.z*f.z)*(v - f);
				}
			}
		}
		gvField = newGvField;
	}

	if (gvField.size() != vectorX->size() ||
		gvField.size() != vectorY->size() ||
		gvField.size() != vectorZ->size())
	{
		return false;
	}

	for (size_t index = 0; index < gvField.size(); ++index)
	{
		(*vectorX)[index] = gvField[index].x;
		(*vectorY)[index] = gvField[index].y;
		(*vectorZ)[index] = gvField[index].z;

		//short length = 10000 * std::sqrt(static_cast<double>(gvField[index].x * gvField[index].x + gvField[index].y * gvField[index].y + gvField[index].z * gvField[index].z));
	}
	
    return( true );
}
