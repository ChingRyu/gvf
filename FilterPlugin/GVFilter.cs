﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Pluto;

namespace Plugin
{
	[Category( "CL.Wang" )]
	[PluginType( PluginCategory.Filter )]
	public partial class GVFilter : Form, ICommonPlugin, IMenuEx
	{
        #region Callback Function
        ProgressCallback messageOutCallback = (value) =>
        {
            Console.WriteLine(value);
        };
        #endregion 

		#region コンストラクタ
		public GVFilter( )
		{
			InitializeComponent( );
		}
		#endregion

		#region フィールド
		DataManager dataManager = null;
		IPluto iPluto = null;
		#endregion

		#region IPlugin メンバ
		string IPlugin.Author { get { return ( "PLUTO Development Team" ); } }

		string IPlugin.Text { get { return ( "GVF" ); } }

		string IPlugin.Comment { get { return ( "Generate gradient vector flow" ); } }

		bool IPlugin.Initialize( DataManager data, IPluto pluto )
		{
			// DataManager および IPluto を取得する．
			dataManager = data;
			iPluto = pluto;

			return ( true );
		}
		#endregion

		#region ICommonPlugin メンバ
		object ICommonPlugin.Run( params object[] args )
		{
			// DataManager および IPluto の取得に失敗している．または，画像が選択されていない．
			if( dataManager == null || iPluto == null || dataManager.Active == null )
			{
				return ( null );
			}

            if (this.ShowDialog() == DialogResult.OK)
            {
                int iters = (int)this.numericIter.Value;
                float mu = (float)this.numericMu.Value;

                Mist.MistArray originImage = dataManager.Active;
                Mist.MistArrayF vectorX = new Mist.MistArrayF(originImage.Size1, originImage.Size2, originImage.Size3,
                                                              originImage.Reso1, originImage.Reso2, originImage.Reso3);
                Mist.MistArrayF vectorY = new Mist.MistArrayF(originImage.Size1, originImage.Size2, originImage.Size3,
                                                              originImage.Reso1, originImage.Reso2, originImage.Reso3);
                Mist.MistArrayF vectorZ = new Mist.MistArrayF(originImage.Size1, originImage.Size2, originImage.Size3,
                                                              originImage.Reso1, originImage.Reso2, originImage.Reso3);

                bool result = GradientVectorFlow(messageOutCallback, originImage.Image, vectorX.Image, vectorY.Image, vectorZ.Image, iters, mu);
            			
            }

			return ( null );
		}  
		#endregion

		#region IMenuEx メンバ
		bool IMenuEx.Visible
		{
			get
			{
				// DataManager が設定されている場合は表示，されていない場合は非表示とする．
				return ( dataManager != null );
			}
		}

		bool IMenuEx.Enabled
		{
			get
			{
				if( dataManager == null )
				{
					// DataManager が設定されていない場合は無効とする．
					return ( false );
				}
				else
				{
					// Active なデータがある場合は有効，ない場合は無効とする．
					return ( dataManager.Active != null );
				}
			}
		}

		string IMenuEx.Category
		{
			get
			{
				return ( "CL.Wang" );
			}
		}
		#endregion

		#region イベント
		private void OnOK( object sender, EventArgs e )
		{
			this.DialogResult = DialogResult.OK;
		}

		private void OnCancel( object sender, EventArgs e )
		{
			this.DialogResult = DialogResult.Cancel;
		}
		#endregion

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        internal delegate void ProgressCallback(string errorMessage);

        [DllImport("GVF.dll", EntryPoint = "Generate")]
        internal static extern bool GradientVectorFlow([MarshalAs(UnmanagedType.FunctionPtr)]ProgressCallback callbackPointer, IntPtr inputImage, IntPtr vectorfieldX, IntPtr vectorfieldY, IntPtr vectorfieldZ, int iters, float mu);

        private void GVFilter_Load(object sender, EventArgs e)
        {

        }
	}
}