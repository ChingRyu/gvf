﻿namespace Plugin
{
	partial class GVFilter
	{
		/// <summary>
		/// 必要なデザイナ変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose( );
			}
			base.Dispose( disposing );
		}

		#region Windows フォーム デザイナで生成されたコード

		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent( )
		{
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numericIter = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericMu = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericIter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMu)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(58, 68);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(81, 23);
            this.buttonCancel.TabIndex = 21;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.OnCancel);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(8, 68);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(44, 23);
            this.buttonOK.TabIndex = 22;
            this.buttonOK.Text = "&OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.OnOK);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "Iter:";
            // 
            // numericIter
            // 
            this.numericIter.Location = new System.Drawing.Point(47, 7);
            this.numericIter.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericIter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericIter.Name = "numericIter";
            this.numericIter.Size = new System.Drawing.Size(75, 19);
            this.numericIter.TabIndex = 24;
            this.numericIter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericIter.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "mu:";
            // 
            // numericMu
            // 
            this.numericMu.DecimalPlaces = 1;
            this.numericMu.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.numericMu.Location = new System.Drawing.Point(47, 37);
            this.numericMu.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMu.Name = "numericMu";
            this.numericMu.Size = new System.Drawing.Size(75, 19);
            this.numericMu.TabIndex = 26;
            this.numericMu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericMu.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // GVFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(145, 102);
            this.ControlBox = false;
            this.Controls.Add(this.numericMu);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericIter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "GVFilter";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Parameters:";
            this.Load += new System.EventHandler(this.GVFilter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericIter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericIter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericMu;
	}
}